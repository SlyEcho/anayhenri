function log(m) {
    console.log(`${new Date().toISOString()}: ${m}`)
}

export default {
    lowMemoryMode: true,
    fetch(req) {
        const url = new URL(req.url)
        let path = url.pathname
        if (url.pathname === '/' || url.pathname === '/about') {
            path = '/index.html'
        }

        log(`${req.method} ${path}`)

        if (req.method === 'GET') {
            return new Response(Bun.file("dist" + path))
        }

        return new Response(`404 Not Found: ${path}`, { status: 404 })
    },

    error(e) {
        log(`error: ${e.code}: ${e.message}`)

        if (e.code === 'ENOENT') {
            return new Response(`404 Not Found: ${e.path}`, { status: 404 })
        }
    }
}
