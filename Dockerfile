# docker buildx build . --platform linux/amd64,linux/arm64 --tag cumtr.ee/henri/anayhenri --push

FROM --platform=$BUILDPLATFORM oven/bun:latest AS build

WORKDIR /src
COPY ["package.json", "bun.lockb", "./"]

RUN  --mount=type=cache,target=/root/.bun/install/cache \
    bun install

COPY . .
RUN bun vite build

FROM --platform=${BUILDPLATFORM} alpine AS tini-binary
ENV TINI_VERSION=v0.19.0
# Use BuildKit to help translate architecture names
ARG TARGETPLATFORM
# translating Docker's TARGETPLATFORM into tini download names
RUN case ${TARGETPLATFORM} in \
         "linux/amd64")  TINI_ARCH=amd64  ;; \
         "linux/arm64")  TINI_ARCH=arm64  ;; \
         "linux/arm/v7") TINI_ARCH=armhf  ;; \
         "linux/arm/v6") TINI_ARCH=armel  ;; \
         "linux/386")    TINI_ARCH=i386   ;; \
    esac \
 && wget -q https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini-static-${TINI_ARCH} -O /tini \
 && chmod +x /tini

FROM oven/bun:latest AS deploy

WORKDIR /app
COPY --from=build /src/dist ./dist
COPY server.js .

ENV NODE_PORT=3000
ENV NODE_ENV=production

EXPOSE 3000

COPY --from=tini-binary /tini /tini
ENTRYPOINT ["/tini", "--"]

CMD ["bun", "run", "--no-install", "--smol", "./server.js"]
