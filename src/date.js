export function utc(d) {
  return {
    y: d.getUTCFullYear(),
    m: d.getUTCMonth() + 1,
    d: d.getUTCDate(),
    h: d.getUTCHours(),
    n: d.getUTCMinutes(),
    s: d.getUTCSeconds(),
  };
}

function is_leap_year(y) {
  if ((y % 4) != 0) return false
  else if ((y % 100) != 0) return true
  else if ((y % 400) != 0) return false
  else return false
}

const lengths = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

function month_length({ y, m }) {
  if (m == 2 && is_leap_year(y)) {
    return 29
  }
  return lengths[m - 1]
}

function prev_month({ y, m }) {
  m -= 1
  if (m <= 0) {
    m = 12
    y -= 1
  }
  return { y, m }
}

export function diff(a, b) {

  let [s, n, h, d, m, y] = [
    b.s - a.s, b.n - a.n, b.h - a.h,
    b.d - a.d, b.m - a.m, b.y - a.y,
  ]

  if (s < 0) {
    s += 60
    n -= 1
  }

  if (n < 0) {
    n += 60
    h -= 1
  }

  if (h < 0) {
    h += 24
    d -= 1
  }

  if (d < 0) {
    d += month_length(prev_month(b))
    m -= 1
  }

  if (m < 0) {
    m += 12
    y -= 1
  }

  if (y < 0) {
    return diff(b, a)
  }

  return {
    y, m, d, h, n, s
  };
}
