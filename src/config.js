export default {
  names: ['Ana', 'Henri'],
  dates: [
    { name: '🥰 Together for 🐨', date: new Date('2018-08-07T16:40Z') },
    { name: '💍🖊️ Civil Marriage 🇪🇪', date: new Date('2022-01-19T14:34Z') },
    { name: '👰🏽🤵🏻 Church Marriage 🇲🇽', date: new Date('2022-02-19T23:00Z') },
  ],
  pics: [
    '/pics/IMG_3177.jpg',
    '/pics/IMG_2810.jpg',
    '/pics/IMG_3243.jpg',
    '/pics/IMG_3018.jpg',
    '/pics/7DA7F188-D7C8-4972-9147-86EFA1A75533.jpg',
  ],
};